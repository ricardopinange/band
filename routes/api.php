<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'musics'], function () {
    Route::get('/list', 'MusicsController@index');
    Route::get('/edit/{id}', 'MusicsController@edit');
    Route::post('/insert', 'MusicsController@store');
    Route::post('/update/{id}', 'MusicsController@update');
    Route::delete('/delete/{id}', 'MusicsController@destroy');
});

Route::group(['prefix' => 'lyrics'], function () {
    Route::get('/list', 'LyricsController@index');
    Route::get('/edit/{id}', 'LyricsController@edit');
    Route::post('/insert', 'LyricsController@store');
    Route::post('/update/{id}', 'LyricsController@update');
    Route::delete('/delete/{id}', 'LyricsController@destroy');
});

Route::group(['prefix' => 'albums'], function () {
    Route::get('/list', 'AlbumsController@index');
    Route::get('/edit/{id}', 'AlbumsController@edit');
    Route::post('/insert', 'AlbumsController@store');
    Route::post('/update/{id}', 'AlbumsController@update');
    Route::delete('/delete/{id}', 'AlbumsController@destroy');
});

Route::group(['prefix' => 'albums-musics'], function () {
    Route::get('/list', 'AlbumsMusicsController@index');
    Route::post('/insert', 'AlbumsMusicsController@store');
    Route::post('/delete', 'AlbumsMusicsController@destroy');
});
