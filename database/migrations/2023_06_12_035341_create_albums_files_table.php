<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('albums_files', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('albums_id');
            $table->string('path', 500);
            $table->string('type', 30);
            $table->string('extension', 10);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('albums_id')->references('id')->on('albums')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('albums_files');
    }
};
