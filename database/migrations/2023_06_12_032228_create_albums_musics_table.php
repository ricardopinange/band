<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('albums_musics', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('albums_id');
            $table->uuid('musics_id');
            $table->tinyInteger('number')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('albums_id')->references('id')->on('albums')->onDelete('restrict');
            $table->foreign('musics_id')->references('id')->on('musics')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('albums_musics');
    }
};
