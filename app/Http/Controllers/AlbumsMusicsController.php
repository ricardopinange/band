<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Repositories\AlbumsMusicsRepository;
use App\Repositories\AlbumsRepository;
use App\Repositories\MusicsRepository;
use App\Http\Requests\AlbumsMusicsRequest;

class AlbumsMusicsController extends Controller
{
    private $albumsMusicsRepository;
    private $albumsRepository;
    private $musicsRepository;

    public function __construct(
        AlbumsMusicsRepository $albumsMusicsRepository,
        AlbumsRepository $albumsRepository,
        MusicsRepository $musicsRepository
    )
    {
        $this->albumsMusicsRepository = $albumsMusicsRepository;
        $this->albumsRepository = $albumsRepository;
        $this->musicsRepository = $musicsRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = $this->albumsMusicsRepository->all();
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AlbumsMusicsRequest $request)
    {
        try {
            $data = collect([]);

            $album = $this->albumsRepository->find($request['albums_id']);

            if (!isset($album)) {
                $message = "Album not found";
                throw new \Exception($message, Response::HTTP_NOT_FOUND);
            }

            DB::transaction(function () use ($request, $data) {
                collect($request['musics_id'])->map(function($value) use ($request, &$data) {
                    $old = $this->albumsMusicsRepository->validate($request['albums_id'], $value);
                    $music = $this->musicsRepository->find($value);

                    if (!isset($music)) {
                        $message = "Music not found! ID: " . $value;
                        throw new \Exception($message, Response::HTTP_NOT_FOUND);
                    }

                    if (isset($old)) {
                        throw new \Exception('Music already registered in the album! Title: ' . $music->title, Response::HTTP_BAD_REQUEST);
                    }

                    $data->push($this->albumsMusicsRepository->store(
                        [
                            'albums_id' => $request['albums_id'],
                            'musics_id' => $value,
                            'number' => $this->albumsMusicsRepository->lastNumber($request['albums_id'])
                        ]
                    ));
                });
            });

            return $this->responseSuccess($data, 'Musics successfully added in the album');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        try {
            $data = $this->albumsMusicsRepository->find($id);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AlbumsMusicsRequest $request, string $id)
    {
        try {
            $data = $this->albumsMusicsRepository->update($request->validated(), $id);
            return $this->responseSuccess($data, 'Music successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AlbumsMusicsRequest $request)
    {
        try {
            $album = $this->albumsRepository->find($request['albums_id']);

            if (!isset($album)) {
                $message = "Album not found";
                throw new \Exception($message, Response::HTTP_NOT_FOUND);
            }

            DB::transaction(function () use ($request) {
                collect($request['musics_id'])->map(function($value) use ($request) {
                    $albumsMusics = $this->albumsMusicsRepository->findLastOneByFilter(
                        [
                            'albums_id' => $request['albums_id'],
                            'musics_id' => $value
                        ]
                    );

                    if (!isset($albumsMusics)) {
                        $message = "Music not found! ID: " . $value;
                        throw new \Exception($message, Response::HTTP_NOT_FOUND);
                    }

                    $this->albumsMusicsRepository->destroy($albumsMusics->id);
                });

                $this->albumsMusicsRepository->reorderNumber($request['albums_id']);
            });

            return $this->responseSuccess([], 'Musics successfully deleted from the album');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
