<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Repositories\AlbumsFilesRepository;
use App\Repositories\AlbumsRepository;
use App\Http\Requests\AlbumsRequest;

class AlbumsController extends Controller
{
    private $albumsFilesRepository;
    private $albumsRepository;

    public function __construct(
        AlbumsFilesRepository $albumsFilesRepository,
        AlbumsRepository $albumsRepository
    )
    {
        $this->albumsFilesRepository = $albumsFilesRepository;
        $this->albumsRepository = $albumsRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = $this->albumsRepository->all();
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AlbumsRequest $request)
    {
        try {
            $data = null;
            DB::transaction(function () use ($request, &$data) {
                $data = $this->albumsRepository->store($request->validated());
                $dataFile = [
                    'albums_id' => $data->id
                ];
                $this->saveFile($request, $dataFile);
            });
            return $this->responseSuccess($data, 'Album successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        try {
            $data = $this->albumsRepository->find($id);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AlbumsRequest $request, string $id)
    {
        try {
            $data = null;
            DB::transaction(function () use ($request, &$data, $id) {
                $data = $this->albumsRepository->update($request->validated(), $id);
                $dataFile = [
                    'albums_id' => $id
                ];
                $this->saveFile($request, $dataFile);
            });
            return $this->responseSuccess($data, 'Album successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $this->albumsRepository->destroy($id);
            return $this->responseSuccess([], 'Album successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Save file
     *
     * @param array $data
     * @return array
     */
    private function saveFile($request, $data)
    {
        if ($request->hasFile('image')) {
			$allowed_extensions = ['jpg','jpeg','png','gif','svg','webp'];
			$path = "/albums/{$data['albums_id']}";

			if ($request->hasFile('image')) {
				$file = $request->file('image');
				$mime_type = $file->getClientMimeType();
                $file_extension = $file->extension();
                $file = file_get_contents($file);
			}

            if (!in_array($file_extension, $allowed_extensions)) {
				$message = "The file must be of type: " . implode(',', $allowed_extensions) . ".";
				throw new \Exception($message, Response::HTTP_BAD_REQUEST);
			}

			$data['path'] = $path;
            $data['type'] = $mime_type;
            $data['extension'] = $file_extension;

			$oldAlbumsFiles = $this->albumsFilesRepository->findLastOneByFilter([
                'albums_id' => $data['albums_id']
            ]);

            if (!$oldAlbumsFiles) {
				$albumsFiles = $this->albumsFilesRepository->store($data);
            } else {
                $albumsFiles = $this->albumsFilesRepository->update($data, $oldAlbumsFiles->id);
                if ($oldAlbumsFiles->extension != $data['extension']) {
                    $file_old = "{$path}/{$oldAlbumsFiles->id}.{$oldAlbumsFiles->extension}";
                    Storage::disk('public')->exists($file_old)
                    ? Storage::disk('public')->delete($file_old)
                    : null;
                }
            }

			$file_name = "{$path}/{$albumsFiles->id}.{$file_extension}";

            if (!Storage::disk('public')->put($file_name, $file)) {
                throw new \Exception('Could not save file', Response::HTTP_INTERNAL_SERVER_ERROR);
			}

			return $albumsFiles;
        }
    }
}
