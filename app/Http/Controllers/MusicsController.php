<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\MusicsRepository;
use App\Http\Requests\MusicsRequest;

class MusicsController extends Controller
{
    private $musicsRepository;

    public function __construct(MusicsRepository $musicsRepository)
    {
        $this->musicsRepository = $musicsRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = $this->musicsRepository->all();
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MusicsRequest $request)
    {
        try {
            $data = $this->musicsRepository->store($request->validated());
            return $this->responseSuccess($data, 'Music successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        try {
            $data = $this->musicsRepository->find($id);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MusicsRequest $request, string $id)
    {
        try {
            $data = $this->musicsRepository->update($request->validated(), $id);
            return $this->responseSuccess($data, 'Music successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $this->musicsRepository->destroy($id);
            return $this->responseSuccess([], 'Music successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
