<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\LyricsRepository;
use App\Http\Requests\LyricsRequest;

class LyricsController extends Controller
{
    private $lyricsRepository;

    public function __construct(LyricsRepository $lyricsRepository)
    {
        $this->lyricsRepository = $lyricsRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = $this->lyricsRepository->all();
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(LyricsRequest $request)
    {
        try {
            $data = $this->lyricsRepository->store($request->validated());
            return $this->responseSuccess($data, 'Lyric successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        try {
            $data = $this->lyricsRepository->find($id);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(LyricsRequest $request, string $id)
    {
        try {
            $data = $this->lyricsRepository->update($request->validated(), $id);
            return $this->responseSuccess($data, 'Lyric successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $this->lyricsRepository->destroy($id);
            return $this->responseSuccess([], 'Lyric successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
