<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasUuid;

class AlbumFile extends Model
{
    use HasFactory, HasUuid, SoftDeletes;

    protected $table = 'albums_files';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $appends = ['link'];
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'albums_id',
        'path',
        'type',
        'extension'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['path'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [];

    public function albums() {
        return $this->belongsTo(Album::class);
    }

    public function getLinkAttribute()
    {
        return url('/')."/storage{$this->path}/{$this->id}.{$this->extension}";
    }

}
