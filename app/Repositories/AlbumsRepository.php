<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Album;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class AlbumsRepository implements RepositoryInterface
{
    public $request;

    public function __construct()
    {
        $this->request = app(Request::class);
    }

    public function all()
    {
        $query = Album::select(
                'albums.*',
                DB::raw('(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(musics.length))) FROM musics
                          INNER JOIN albums_musics am ON am.musics_id = musics.id
                          WHERE am.albums_id = albums.id
                          AND am.deleted_at IS NULL) AS length')
            )
            ->with([
                'albums_files',
                'albums_musics' => function($q) {
                    $q->orderBy('number', 'asc');
                },
                'albums_musics.musics:id,title,length'
            ]);

        if (isset($this->request->search)) {
            $query->where('title', 'LIKE', '%' . $this->request->search . '%');
        }

        if (isset($this->request->id)) {
            $query->where('id', $this->request->id);
        }

        if (isset($this->request->start_release_date)) {
            $query->where('released_at', '>=', $this->request->start_release_date);
        }

        if (isset($this->request->end_release_date)) {
            $query->where('released_at', '<=', $this->request->end_release_date);
        }

        if (isset($this->request->per_page)) {
            $per_page = in_array($this->request->per_page, [12,24,36])
                ? $this->request->per_page
                : 12;
        }

        $data = $query->orderBy('albums.released_at', 'ASC')
            ->paginate($per_page ?? 12);

        return $data;
    }

    public function store($data)
    {
        if ($this->validate($data['title'])) {
            $message = "Album already registered";
            throw new \Exception($message, Response::HTTP_BAD_REQUEST);
        }

        $id = Album::create($data)->id;
        return Album::find($id);
    }

    public function find($id)
    {
        return Album::find($id);
    }

    public function findLastOneByFilter(array $filter)
    {
        return Album::where($filter)->latest()->first();
    }

    public function update($data, $id)
    {
        $album = Album::find($id);

        if (!isset($album)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        if ($this->validate($data['title'], $id)) {
            $message = "Album already registered";
            throw new \Exception($message, Response::HTTP_BAD_REQUEST);
        }

        $album->update($data);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = Album::find($id);

        if (!isset($data)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        $data->delete();
    }

    public function validate(string $title, string $id = NULL)
    {
        $query = Album::where('title', $title);

        if (isset($id)) {
            $query->where('id', '!=', $id);
        }

        return $query->first();
    }
}
