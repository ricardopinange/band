<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Music;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MusicsRepository implements RepositoryInterface
{
    public $request;

    public function __construct()
    {
        $this->request = app(Request::class);
    }

    public function all()
    {
        $query = Music::select();

        if (isset($this->request->search)) {
            $query->where('title', 'LIKE', '%' . $this->request->search . '%');
        }

        if (isset($this->request->start_release_date)) {
            $query->where('released_at', '>=', $this->request->start_release_date);
        }

        if (isset($this->request->end_release_date)) {
            $query->where('released_at', '<=', $this->request->end_release_date);
        }

        if (isset($this->request->per_page)) {
            $per_page = in_array($this->request->per_page, [10,25,50])
                ? $this->request->per_page
                : 10;
        }

        $data = $query->orderBy('title', 'ASC')->paginate($per_page ?? 10);

        return $data;
    }

    public function store($data)
    {
        $id = Music::create($data)->id;
        return Music::find($id);
    }

    public function find($id)
    {
        return Music::find($id);
    }

    public function findLastOneByFilter(array $filter)
    {
        return Music::where($filter)->latest()->first();
    }

    public function update($data, $id)
    {
        Music::find($id)->update($data);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = Music::find($id);

        if (!isset($data)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        $data->delete();
    }

    public function validate(string $name, string $id = NULL)
    {
        $query = Music::where('name', $name);

        if (isset($id)) {
            $query->where('id', '!=', $id);
        }

        return $query->first();
    }
}
