<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\AlbumFile;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AlbumsFilesRepository implements RepositoryInterface
{
    public $request;

    public function __construct()
    {
        $this->request = app(Request::class);
    }

    public function all()
    {
        $query = AlbumFile::select()->latest();
        return $data;
    }

    public function store($data)
    {
        $id = AlbumFile::create($data)->id;
        return AlbumFile::find($id);
    }

    public function find($id)
    {
        return AlbumFile::find($id);
    }

    public function findLastOneByFilter(array $filter)
    {
        return AlbumFile::where($filter)->latest()->first();
    }

    public function update($data, $id)
    {
        AlbumFile::find($id)->update($data);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = AlbumFile::find($id);

        if (!isset($data)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        $data->delete();
    }

}
