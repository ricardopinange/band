<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Lyrics;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LyricsRepository implements RepositoryInterface
{
    public $request;

    public function __construct()
    {
        $this->request = app(Request::class);
    }

    public function all()
    {
        $query = Lyrics::select();

        if (isset($this->request->musics_id)) {
            $query->where('musics_id', $this->request->musics_id);
        }

        if (isset($this->request->per_page)) {
            $per_page = in_array($this->request->per_page, [10,25,50])
                ? $this->request->per_page
                : 10;
        }

        $data = $query->orderBy('created_at', 'ASC')->paginate($per_page ?? 10);

        return $data;
    }

    public function store($data)
    {
        if ($this->validate($data['musics_id'])) {
            $message = "Music already used in another register";
            throw new \Exception($message, Response::HTTP_BAD_REQUEST);
        }

        $id = Lyrics::create($data)->id;
        return Lyrics::find($id);
    }

    public function find($id)
    {
        return Lyrics::find($id);
    }

    public function findLastOneByFilter(array $filter)
    {
        return Lyrics::where($filter)->latest()->first();
    }

    public function update($data, $id)
    {
        $lyrics = Lyrics::find($id);
        
        if (!isset($lyrics)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        if ($this->validate($data['musics_id'], $id)) {
            $message = "Music already used in another register";
            throw new \Exception($message, Response::HTTP_BAD_REQUEST);
        }

        $lyrics->update($data);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = Lyrics::find($id);

        if (!isset($data)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        $data->delete();
    }

    public function validate(string $musics_id, string $id = NULL)
    {
        $query = Lyrics::where('musics_id', $musics_id);

        if (isset($id)) {
            $query->where('id', '!=', $id);
        }

        return $query->first();
    }
}
