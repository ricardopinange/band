<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\AlbumMusic;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AlbumsMusicsRepository implements RepositoryInterface
{
    public $request;

    public function __construct()
    {
        $this->request = app(Request::class);
    }

    public function all()
    {
        $query = AlbumMusic::select('albums_musics.*')
            ->with([
                'albums:id,title,released_at',
                'musics:id,title,length',
            ])
            ->join('albums', 'albums_musics.albums_id', '=', 'albums.id')
            ->join('musics', 'albums_musics.musics_id', '=', 'musics.id');

        if (isset($this->request->search)) {
            $search = $this->request->search;
            $query->where(function ($query) use ($search) {
                $query->where('albums.title', 'LIKE', '%' . $search . '%')
                    ->orWhere('musics.title', 'LIKE', '%' . $search . '%');
            });
        }

        if (isset($this->request->albums_id)) {
            $query->where('albums_id', $this->request->albums_id);
        }

        if (isset($this->request->musics_id)) {
            $query->where('musics_id', $this->request->musics_id);
        }

        if (isset($this->request->per_page)) {
            $per_page = in_array($this->request->per_page, [10,25,50,100])
                ? $this->request->per_page
                : 50;
        }

        $data = $query->orderBy('albums.released_at', 'ASC')
            ->orderBy('albums_musics.number', 'ASC')
            ->paginate($per_page ?? 50);

        return $data;
    }

    public function store($data)
    {
        $id = AlbumMusic::create($data)->id;
        return AlbumMusic::find($id);
    }

    public function find($id)
    {
        return AlbumMusic::find($id);
    }

    public function findLastOneByFilter(array $filter)
    {
        return AlbumMusic::where($filter)->latest()->first();
    }

    public function update($data, $id)
    {
        AlbumMusic::find($id)->update($data);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = AlbumMusic::find($id);

        if (!isset($data)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        $data->delete();
    }

    public function validate(string $albums_id, string $musics_id, string $id = NULL)
    {
        $query = AlbumMusic::where('albums_id', $albums_id)
            ->where('musics_id', $musics_id);

        if (isset($id)) {
            $query->where('id', '!=', $id);
        }

        return $query->first();
    }

    public function lastNumber(string $albums_id)
    {
        $query = AlbumMusic::selectRaw('MAX(number)+1 AS number')
            ->where('albums_id', $albums_id)
            ->first();

        return $query->number ?? 1;
    }

    public function reorderNumber(string $albums_id)
    {
        $query = AlbumMusic::select('id')
            ->where('albums_id', $albums_id)
            ->orderBy('number', 'ASC')
            ->get();

        $number = 1;
        collect($query)->map(function($row) use (&$number) {
            AlbumMusic::find($row->id)->update(['number' => $number]);
            $number++;
        });
    }
}
