<?php

namespace App\GraphQL\Types;

use App\Models\AlbumFile;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AlbumFileType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'AlbumFile',
        'description'   => 'A album file',
        'model'         => AlbumFile::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'description' => 'The album file id',
            ],
            'albums_id' => [
                'type' => Type::string(),
                'description' => 'The album ID',
            ],
            'path' => [
                'type' => Type::string(),
                'description' => 'The path of the album file',
            ],
            'type' => [
                'type' => Type::string(),
                'description' => 'The type of the album file',
            ],
            'extension' => [
                'type' => Type::string(),
                'description' => 'The extension of the album file',
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'The created date of the album file',
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'The updated date of the album file',
            ]
        ];
    }
}