<?php

namespace App\GraphQL\Types;

use App\Models\Music;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MusicType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Music',
        'description'   => 'A music',
        'model'         => Music::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'description' => 'The uuid of the music',
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of the music',
            ],
            'length' => [
                'type' => Type::string(),
                'description' => 'The length of the music',
            ],
            'released_at' => [
                'type' => Type::string(),
                'description' => 'The released date of the music',
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'The created date of the music',
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'The updated date of the music',
            ]
        ];
    }
}