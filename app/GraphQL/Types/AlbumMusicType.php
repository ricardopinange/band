<?php

namespace App\GraphQL\Types;

use App\Models\AlbumMusic;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AlbumMusicType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'AlbumMusic',
        'description'   => 'A album music',
        'model'         => AlbumMusic::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'description' => 'The album music id',
            ],
            'albums_id' => [
                'type' => Type::string(),
                'description' => 'The album ID',
            ],
            'musics_id' => [
                'type' => Type::string(),
                'description' => 'The music ID',
            ],
            'number' => [
                'type' => Type::string(),
                'description' => 'The music number on the album',
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'The created date of the album music',
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'The updated date of the album music',
            ]
        ];
    }
}