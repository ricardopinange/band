<?php

namespace App\GraphQL\Types;

use App\Models\Album;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AlbumType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Album',
        'description'   => 'A album',
        'model'         => Album::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'description' => 'The id of the album',
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of the album',
            ],
            'released_at' => [
                'type' => Type::string(),
                'description' => 'The released at of the album',
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'The created date of the album',
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'The updated date of the album',
            ]
        ];
    }
}