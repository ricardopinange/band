<?php

namespace App\GraphQL\Types;

use App\Models\Lyrics;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class LyricsType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Lyrics',
        'description'   => 'A lyrics',
        'model'         => Lyrics::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'description' => 'The uuid of the lyrics',
            ],
            'musics_id' => [
                'type' => Type::string(),
                'description' => 'The musics ID of the lyrics',
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of the lyrics',
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'The created date of the lyrics',
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'The updated date of the lyrics',
            ]
        ];
    }
}