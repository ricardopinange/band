<?php
namespace App\GraphQL\Queries;

use Closure;
use App\Models\Music;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class MusicsQuery extends Query
{
    protected $attributes = [
        'name' => 'musics',
    ];

    public function type(): Type
    {
        return Type::nonNull(Type::listOf(Type::nonNull(GraphQL::type('Music'))));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string(),
            ],
            'length' => [
                'name' => 'length', 
                'type' => Type::string(),
            ],
            'released_at' => [
                'name' => 'released_at', 
                'type' => Type::string(),
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args['id'])) {
            return Music::where('id' , $args['id'])->get();
        }

        if (isset($args['title'])) {
            return Music::where('title', $args['title'])->get();
        }

        return Music::all();
    }
}