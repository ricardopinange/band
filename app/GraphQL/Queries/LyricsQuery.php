<?php
namespace App\GraphQL\Queries;

use Closure;
use App\Models\Lyrics;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class LyricsQuery extends Query
{
    protected $attributes = [
        'name' => 'lyrics',
    ];

    public function type(): Type
    {
        return Type::nonNull(Type::listOf(Type::nonNull(GraphQL::type('Lyrics'))));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
            ],
            'musics_id' => [
                'name' => 'musics_id',
                'type' => Type::string(),
            ],
            'description' => [
                'name' => 'description', 
                'type' => Type::string(),
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args['id'])) {
            return Lyrics::where('id' , $args['id'])->get();
        }

        return Lyrics::all();
    }
}