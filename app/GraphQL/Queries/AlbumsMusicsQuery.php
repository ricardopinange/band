<?php
namespace App\GraphQL\Queries;

use Closure;
use App\Models\AlbumMusic;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class AlbumsMusicsQuery extends Query
{
    protected $attributes = [
        'name' => 'albums_musics',
    ];

    public function type(): Type
    {
        return Type::nonNull(Type::listOf(Type::nonNull(GraphQL::type('AlbumMusic'))));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
            ],
            'albums_id' => [
                'name' => 'albums_id',
                'type' => Type::string(),
            ],
            'musics_id' => [
                'name' => 'musics_id', 
                'type' => Type::string(),
            ],
            'number' => [
                'name' => 'number', 
                'type' => Type::string(),
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args['id'])) {
            return AlbumMusic::where('id' , $args['id'])->get();
        }

        return AlbumMusic::all();
    }
}