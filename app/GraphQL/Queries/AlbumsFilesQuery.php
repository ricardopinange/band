<?php
namespace App\GraphQL\Queries;

use Closure;
use App\Models\AlbumFile;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class AlbumsFilesQuery extends Query
{
    protected $attributes = [
        'name' => 'albums_files',
    ];

    public function type(): Type
    {
        return Type::nonNull(Type::listOf(Type::nonNull(GraphQL::type('AlbumFile'))));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
            ],
            'albums_id' => [
                'name' => 'albums_id',
                'type' => Type::string(),
            ],
            'path' => [
                'name' => 'path', 
                'type' => Type::string(),
            ],
            'type' => [
                'name' => 'type', 
                'type' => Type::string(),
            ],
            'extension' => [
                'name' => 'extension', 
                'type' => Type::string(),
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args['id'])) {
            return AlbumFile::where('id' , $args['id'])->get();
        }

        return AlbumFile::all();
    }
}