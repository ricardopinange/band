<?php

namespace App\GraphQL\Schemas;

use Rebing\GraphQL\Support\Contracts\ConfigConvertible;

class DefaultSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return [
            'query' => [
                \App\GraphQL\Queries\UsersQuery::class,
                \App\GraphQL\Queries\MusicsQuery::class,
                \App\GraphQL\Queries\LyricsQuery::class,
                \App\GraphQL\Queries\AlbumsQuery::class,
                \App\GraphQL\Queries\AlbumsFilesQuery::class,
                \App\GraphQL\Queries\AlbumsMusicsQuery::class,
            ],

            'mutation' => [
            ],

            'types' => [
                \App\GraphQL\Types\UserType::class,
                \App\GraphQL\Types\MusicType::class,
                \App\GraphQL\Types\LyricsType::class,
                \App\GraphQL\Types\AlbumType::class,
                \App\GraphQL\Types\AlbumFileType::class,
                \App\GraphQL\Types\AlbumMusicType::class,
            ],

            'middleware' => null,

            'method' => ['GET', 'POST'],

            'execution_middleware' => null,
        ];
    }
}
